from py_stealth.methods import *
from Helpers.misc import Misc

if __name__ == "__main__":
    AutoMenu("category", "(first)")
    AutoMenu("creature", "(first)")
    while Misc.connected_and_alive():
        CloseMenu()
        CancelMenu()
        UseSkill("Tracking")
        Wait(11100)
