import os
from datetime import datetime, timedelta
from py_stealth.methods import *
from Helpers.logger import Logger
from Helpers.tiles import Tiles
from Helpers.items import Items
from Helpers.config import Config
from Helpers.misc import Misc

TO_BANK = True
BANK = (1430, 1653)
POINT = (1411, 1671)
SCRIPT_DIR = os.path.abspath(os.path.dirname(__file__))
CONFIG_DIR = "/".join(SCRIPT_DIR.split('/')[:-1]) + "/Configs"

def fishing(tile_id: int, tile_x: int, tile_y: int, tile_z: int) -> None:
    """Fishes in target tile until depletion

    Args:
        tile_id (int): Target tile ID
        tile_x (int): Target tile X
        tile_y (int): Target tile Y
        tile_z (int): Target tile Z
    """

    logger.debug(f"{tile_x=} {tile_y=}")
    while Misc.connected_and_alive():
        if Weight() >= MaxWeight() - 80:
            if newMoveXY(BANK[0], BANK[1], True, 0, True):
                if Misc.open_bank():
                    logger.info("Bank opened")
                    Items.move_type_to_container(types.get_value("fish_steak"),
                        Backpack(),
                        ObjAtLayer(BankLayer()),
                        -1
                    )
                    newMoveXY(POINT[0], POINT[1], True, 0, True)
                else:
                    logger.error("Failed to open bank")
            else:
                logger.error("Failed to reach bank")

        if Items.type_exists(types.get_value("fishing_pole")):
            UseType(types.get_value("fishing_pole"), 0xFFFF)
            if WaitForTarget(2000):
                started = datetime.now()
                WaitTargetTile(tile_id, tile_x, tile_y, tile_z)
                WaitJournalLine(started, "|".join(
                    messages.get_value("continue_harvesting") +
                    messages.get_value("stop_harvesting")
                ), 30000)

                if datetime.now() >= started + timedelta(seconds=30):
                    logger.info("Waiting for message timed out")
                    break

                if InJournalBetweenTimes("|".join(messages.get_value("stop_harvesting")),
                                        started, datetime.now()) > 0:
                    logger.debug(f"Skipping tile: {tile_x=} {tile_y=}")
                    break
            else:
                logger.debug("Failed to get target from pole")
        else:
            # TODO: Get some Polez
            logger.info("No more poles left")
            exit(1)

        # Should be used because of server delay =(
        #Wait(500)
if __name__ == "__main__":
    # Init required classes
    logger = Logger().get_logger()
    types = Config(f"{CONFIG_DIR}/Generic/types.yaml")
    messages = Config(f"{CONFIG_DIR}/Generic/messages.yaml")

    SetMoveOpenDoor(True)
    newMoveXY(POINT[0], POINT[1], True, 0, True)
    fishing_tiles = Tiles.find_tiles(
        GetX(Self()),
        GetY(Self()),
        5,
        types.get_value("water_tiles"))

    logger.info(fishing_tiles)
    while Misc.connected_and_alive():
        for tile in fishing_tiles:
            fishing(*tile)
