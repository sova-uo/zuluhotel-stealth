from py_stealth.methods import *
from Helpers.misc import Misc


WEAPONS = [0x13FE, 0x1401, 0x0F5C]
if __name__ == "__main__":
    while Misc.connected_and_alive():
        if not ObjAtLayer(RhandLayer()):
            for weapon in WEAPONS:
                if FindType(weapon, Backpack()):
                    AddToSystemJournal("Rearmed!")
                    Equip(RhandLayer(), FindItem())
                    Wait(2000)
                    break
        else:
            Wait(10000)
