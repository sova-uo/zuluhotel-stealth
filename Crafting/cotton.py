import os
from datetime import datetime
from py_stealth.methods import *
from Helpers.logger import Logger
from Helpers.config import Config
from Helpers.misc import Misc
from Helpers.items import Items

SCRIPT_DIR = os.path.abspath(os.path.dirname(__file__))
CONFIG_DIR = "/".join(SCRIPT_DIR.split('/')[:-1]) + "/Configs"

BANK = (1430, 1653)
POINT = (1221, 1723)


if __name__ == "__main__":
    # Init required classes
    logger = Logger().get_logger()
    types = Config(f"{CONFIG_DIR}/Generic/types.yaml")
    messages = Config(f"{CONFIG_DIR}/Generic/messages.yaml")

    SetFindDistance(40)
    SetMoveOpenDoor(True)
    newMoveXY(POINT[0], POINT[1], True, 0, True)
    if FindType(types.get_value("cotton_plant"), Ground()):
        logger.info(f"Found {len(GetFoundList())} plants")
        for plant in GetFoundList():
            while IsObjectExists(plant):
                newMoveXY(
                    GetX(plant),
                    GetY(plant),
                    True,
                    1,
                    True
                )
                started = datetime.now()
                UseObject(plant)
                WaitJournalLine(started, "Failed|Success|You have|That has already", 5000)
                if Weight() >= MaxWeight() - 80:
                    if newMoveXY(BANK[0], BANK[1], True, 0, True):
                        if Misc.open_bank():
                            logger.info("Bank opened")
                            Items.move_type_to_container(types.get_value("cotton_bush"),
                                Backpack(),
                                ObjAtLayer(BankLayer()),
                                -1
                            )
                            newMoveXY(POINT[0], POINT[1], True, 0, True)
                        else:
                            logger.error("Failed to open bank")
                    else:
                        logger.error("Failed to reach bank")
                # if InJournalBetweenTimes("That has already", started, datetime.now()) > 0:
                #     logger.info("Plant gathered")
                #     break