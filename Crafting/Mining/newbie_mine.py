"""
    Mining.
    Unloads ore to bank.
    Requires separate config files per character
"""
import os
from datetime import datetime, timedelta
from Helpers.logger import Logger
from Helpers.tiles import Tiles
from Helpers.items import Items
from Helpers.config import Config
from Helpers.misc import Misc
from Helpers.telegram import Telegram
from py_stealth.methods import *

SCRIPT_DIR = os.path.abspath(os.path.dirname(__file__))
CONFIG_DIR = "/".join(SCRIPT_DIR.split('/')[:-2]) + "/Configs"
SCRIPT_CONFIG_PATH = f"{CONFIG_DIR}/Crafting/Mining/newbie_mine"

def smelt() -> None:
    """Reach forge and smelt ore"""
    if newMoveXY(forge["x"], forge["y"], True, 0, True):
        if not FindType(forge["type"], Ground()):
            logger.error("Unable to find forge!")
            return
        forge_id = FindItem()

        while FindType(types.get_value("ore")):
            for ore in GetFoundList():
                Misc.cancel_targets()
                started = datetime.now()
                UseObject(ore)
                WaitTargetObject(forge_id)
                WaitJournalLine(started, "Failed|Success", 10000)
    else:
        logger.error("Failed to reach forge!")

def get_to_bank() -> None:
    """Travel to bank using crazy route"""
    # Route to get to bank. Required for crazy Minoc map >_>
    for route_point in bank_config["route"]:
        route_x, route_y = route_point
        logger.debug(f"Heading to route point {route_x=} {route_y=}")
        if not newMoveXY(route_x, route_y, True, 0, True):
            logger.error(f"Failed to reach route point {route_x=} {route_y=}")

    if not newMoveXY(bank_config["x"], bank_config["y"], True, 0, True):
        logger.error("Failed to reach bank")

def get_to_mine() -> None:
    """Travel to mine using reversed crazy route"""
    # Route to get to bank. Required for crazy Minoc map >_>

    route = bank_config["route"]
    route.reverse()

    for route_point in route:
        route_x, route_y = route_point
        logger.debug(f"Heading to route point {route_x=} {route_y=}")
        if not newMoveXY(route_x, route_y, True, 0, True):
            logger.error(f"Failed to reach route point {route_x=} {route_y=}")

    if not newMoveXY(starting_point["x"], starting_point["y"], True, 0, True):
        logger.error("Failed to reach mine")


def unload_to_bank() -> None:
    """Unloads all ingots to bank"""

    # We need this to not repeat route from mine to bank
    # In case of missing tools
    if GetX(Self()) != bank_config["x"] and GetY(Self()) != bank_config["y"]:
        get_to_bank()
    if Misc.open_bank():
        logger.info("Bank opened")
        Items.move_type_to_container(types.get_value("ingots"),
            Backpack(),
            ObjAtLayer(BankLayer()),
            -1
        )
        ingots_in_bank = Items.get_items_summary(types.get_value("ingots"), ObjAtLayer(BankLayer()), "Ingots")
        Telegram.telegram_message(Misc.format_statistics_message_from(ingots_in_bank))
    else:
        logger.error("Failed to open bank")

def craft_tools() -> None:
    """Always keep 2 tinker tools and 4 shovels in pack"""
    tinker_tools = types.get_value("tinker_tools")
    shovel = types.get_value("shovel")
    ingots = types.get_value("ingots")
    if Count(tinker_tools) < 2 or Count(shovel) < 4:
        logger.info("Not enought tools in pack, let's craft some", "DEBUG")
        if not Items.grab_from_container(ingots, ObjAtLayer(BankLayer()), 0x0000, 50):
            logger.error("No ingots left in bank")
            Misc.full_disconnect()

        if not FindType(tinker_tools, Backpack()):
            # After res or something like that
            # TODO: Get from bank
            pass

        while Count(tinker_tools) < 2:
            Items.craft_item(tinker_tools, ingots, "Tools", "Tinker")
            Wait(1000)
        while Count(shovel) < 4:
            Items.craft_item(tinker_tools, ingots, "Tools", "Shovel")
            Wait(1000)

def mine(tile_id: int, tile_x: int, tile_y: int, tile_z: int) -> None:
    """Mine specified tile until depletion

    Args:
        tile_id (int): Tile ID
        tile_x (int): Tile X
        tile_y (int): Tile Y
        tile_z (int): Tile Z
    """
    logger.debug(f"{tile_x=} {tile_y=}")
    # Train Arms lore between spots
    Misc.cancel_targets()
    Wait(1500)
    WaitTargetType(types.get_value("dagger"))
    UseSkill("Arms Lore")
    Wait(1000)
    while Misc.connected_and_alive():
        if Weight() >= MaxWeight() - unload_thresholds["smelting_threshold"]:
            smelt()
            unload_to_bank()
            if Misc.open_bank():
                craft_tools()
            get_to_mine()

        if not newMoveXY(tile_x, tile_y, True, 0, True):
            logger.info(f"Failed to reach tile {tile_x}, {tile_y}")
            break

        if Items.type_exists(types.get_value("shovel")):
            UseType(types.get_value("shovel"), 0xFFFF)
            if WaitForTarget(2000):
                started = datetime.now()
                WaitTargetTile(tile_id, tile_x, tile_y, tile_z)
                WaitJournalLine(started, "|".join(
                    messages.get_value("continue_harvesting") +
                    messages.get_value("stop_harvesting")
                ), 30000)

                if datetime.now() >= started + timedelta(seconds=30):
                    logger.info("Waiting for message timed out")
                    break

                if InJournalBetweenTimes("|".join(messages.get_value("stop_harvesting")),
                                        started, datetime.now()) > 0:
                    logger.debug(f"Skipping tile: {tile_x=} {tile_y=}")
                    break
            else:
                logger.debug("Failed to get target from shovel")
        else:
            logger.info("No more shovels left")
            smelt()
            unload_to_bank()
            if Misc.open_bank():
                craft_tools()
            get_to_mine()

        # Should be used because of server delay =(
        Wait(500)


if __name__ == "__main__":
    SetFindDistance(20)
    SetMoveOpenDoor(True)
    SetARStatus(True)
    SetPauseScriptOnDisconnectStatus(True)
    SetMoveThroughNPC(True)
    SetMoveCheckStamina(False)
    # Init required classes
    logger = Logger().get_logger()
    types = Config(f"{CONFIG_DIR}/Generic/types.yaml")
    messages = Config(f"{CONFIG_DIR}/Generic/messages.yaml")
    character_config = Config(f"{SCRIPT_CONFIG_PATH}/{CharName()}.yaml")
    unload_thresholds = character_config.get_value("unload")
    bank_config = character_config.get_value("bank")
    mineable_tiles = types.get_value("mineable_tiles")
    forge = character_config.get_value("forge")
    # ################################################3
    # Stupid Minoc stairs!!!!
    for bad_tile in bank_config["bad_tiles"]:
        SetBadLocation(bad_tile[0], bad_tile[1])
    # Get to starting point
    starting_point = character_config.get_value("starting_point")
    get_to_mine()
    newMoveXY(starting_point["x"], starting_point["y"], True, 1, True)
    tiles = Tiles.find_tiles(
        GetX(Self()),
        GetY(Self()),
        20,
        list(range(mineable_tiles["start"], mineable_tiles["end"]))
    )

    while Misc.connected_and_alive():
        for tile in tiles:
            mine(*tile)
