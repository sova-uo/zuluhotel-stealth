"""Tile-related helpers"""
from loguru import logger
from py_stealth.methods import WorldNum, GetStaticTilesArray, GetLandTilesArray

class Tiles:
    """Tile-related helpers"""
    @staticmethod
    def find_tiles(center_x: int, center_y: int, radius: int, tiles: list[int]) \
        -> set[int, int, int, int]:
        """Get combined list of static and land tiles

        Args:
            center_x (int): X coordinate of central point
            center_y (int): Y coordinate of central point
            radius (int): Search radius
            tiles (list[int]): List of tiles to search for

        Returns:
            set[int, int, int, int]: Set of found tiles (tile, x, y, z)
        """
        min_x, min_y = center_x-radius, center_y-radius
        max_x, max_y = center_x+radius, center_y+radius
        found_tiles = []

        for tile in tiles:
            found_tiles += GetStaticTilesArray(
                min_x, min_y, max_x, max_y, WorldNum(), tile)

            found_tiles += GetLandTilesArray(
                min_x, min_y, max_x, max_y, WorldNum(), tile)

        logger.info(f"Found {len(found_tiles)} tiles")
        return found_tiles
