"""
    Config module
"""
from typing import Union
import yaml
from loguru import logger

class Config:
    """
        Generic class for config handling
    """

    def __init__(self, config_path: str) -> None:
        with open(config_path, mode = "r", encoding = "utf-8") as file:
            self._config = yaml.safe_load(file)
            self._config_path = config_path
            logger.info(f"Config loaded: {config_path}")

    def get_config(self) -> dict:
        """Get full config as dictionary

        Returns:
            dict: Parsed configs content
        """

        return self._config

    def get_value(self, key: str) -> Union[int, list, str, dict, bool]:
        """Get value by key from config file

        Args:
            key (str): Key

        Raises:
            KeyError: If no key exists in config

        Returns:
            Union[int, list, str, dict, bool]: Value
        """

        if value := self._config.get(key):
            return value

        raise KeyError(f"Unable to get value of {key} from {self._config_path}")
