from py_stealth import *

class Telegram:

    @staticmethod
    def telegram_message(message: str):
        MessengerSetToken(1, "TOKEN")
        MessengerSetConnected(1, True)
        while not MessengerGetConnected(1):
            Wait(100)

        current_script = CurrentScriptPath().split("\\")
        MessengerSendMessage(1, f"[{CharName()}] {message}", 226429461)
        Wait(5000)
        MessengerSetConnected(1, False)