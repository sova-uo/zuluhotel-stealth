""" Items-related helper functions """

from datetime import datetime
from loguru import logger
from Helpers.misc import Misc
from py_stealth.methods import *
import re

class Items:
    """ Items-related helper functions """

    @staticmethod
    def get_item_name(item_serial: int) -> str:
        """Returns name of item

        Args:
            item_serial (int): Item object ID

        Returns:
            str: Parsed item name
        """

        if IsObjectExists(item_serial):
            tooltip = GetTooltip(FindItem())
            return tooltip.split("|")[0].lstrip("a ")

        return ""

    @staticmethod
    def type_exists(item_type: int, container: int = Backpack()) -> bool:
        """Checks if item exists in container

        Args:
            item_type (int): Item type
            container (int, optional): Container. Defaults to Backpack().

        Returns:
            bool: True if more than 1 item exists in container
        """

        if FindType(item_type, container):
            return True
        return False

    @staticmethod
    def move_type_to_container(item_type: int, src_container: int, dst_container: int, qty: int) \
        -> None:
        """Moves all items of defines type to container

        Args:
            item_type (int): Item type
            src_container (int): Source container
            dst_container (int): Target container
            qty (int): How much of item to move.
        """

        UseObject(src_container)
        Wait(1000)
        if FindType(item_type, src_container):
            for item in GetFoundList():
                MoveItem(item, qty, dst_container, 0, 0, 0)
                Wait(2000)

    @staticmethod
    def grab_from_container(item_type: int, container: int, color: int, qty: int) -> bool:
        """Get item(s) from target container

        Args:
            item_type (int): Type of item to get
            container (int): Container with items
            color (int): Items color
            qty (int): Quantity to get

        Returns:
            bool: True if attempt succeeded
        """

        UseObject(container)
        Wait(1000)
        if LastContainer() == container:
            if FindTypeEx(item_type, color, container, False):
                MoveItem(FindItem(), qty, Backpack(), 0, 0, 0)
                Wait(1000)
                return True
        else:
            logger.error("Unable to open container!")
            return False


    @staticmethod
    def craft_item(tool: int, resource: int, category: str, item: str) -> None:
        """Craft desired item

        Args:
            tool (int): Tool type
            resource (int): Resouce type to use while crafting
            category (str): Items category ( Tools )
            item (str): Item name in menu ( Shovel )
        """

        logger.info(f"Crafting {item}", "DEBUG")
        if FindTypeEx(resource, 0x0000, Backpack()):
            started = datetime.now()
            CancelAllMenuHooks()
            Misc.cancel_targets()
            WaitTargetObject(FindItem())
            WaitMenu("What", category)
            WaitMenu("What", item)
            WaitTargetObject(FindItem())
            UseType(tool, -1)
            WaitJournalLine(started, "You stop", 15 * 1000)
        else:
            logger.error("No resource for crafting!")

    @staticmethod
    def get_items_summary(item_type: int, container: int, item_name: str) -> list[tuple[str, int]]:
        """Get qty of items in container as a sorted list of tuples like [("iron": 100), ("copper": 80)]

        Args:
            item_type (int): Type of item to find
            container (int): Container to search in
            item_name (str): Item name prefix or suffix ( ingots, logs, hides )

        Returns:
            list[tuple[str, int]]: List of tuples with items and their qty
        """
        result = {}

        item_name = item_name.lower()
        if FindType(item_type, container):
            for item in GetFoundList():
                tooltip = GetTooltip(item)
                match = re.search(r"^(\d+)\s([\w\s]+)", tooltip)
                if match.group(1) and match.group(2):
                    qty = int(match.group(1))
                    item_name = match.group(2)
                    if result.get(item_name):
                        result[item_name] += qty
                    else:
                        result[item_name] = qty
            # Sort result dictionary descending by value ( item qty )
            result = sorted(result.items(), key = lambda record: record[1], reverse = True)

        return result
