""" Items-related helper functions """
import re
from loguru import logger
from py_stealth.methods import *

class Misc:
    """ Misc helper functions """

    @staticmethod
    def cancel_targets() -> None:
        """Cancels target and target hook
        """
        if TargetPresent():
            CancelTarget()
        CancelWaitTarget()

    @staticmethod
    def connected_and_alive() -> bool:
        """Checks if character connected and alive. If not - waits for state change.

        Returns:
            bool: True if healthcheck passed
        """

        if Connected() and not Dead():
            return True

        while not Connected():
            logger.error("Disconnected from server")
            Wait(1000)

        while Dead():
            logger.error(f"I'm dead at X: {GetX(Self())} Y: {GetX(Self())}")
            Wait(1000)

    @staticmethod
    def open_bank() -> bool:
        """Tries to open bank. If attempts > 10 - fails.

        Returns:
            bool: True if bank was opened
        """

        attempt = 0
        while LastContainer() != Backpack():
            UseObject(Backpack())
            Wait(1000)

        while LastContainer() != ObjAtLayer(BankLayer()):
            attempt += 1
            if attempt >= 10:
                return False
            UOSay("bank")
            Wait(1000)

        return LastContainer() == ObjAtLayer(BankLayer())

    @staticmethod
    def full_disconnect():
        """Turn off AR, disconnect and shutdown script"""
        SetARStatus(False)
        Disconnect()
        exit(1)

    @staticmethod
    def format_statistics_message_from(input_list: list[tuple[str, int]]) -> str:
        result = "\n"
        for item in input_list:
            result += f"{item[0].title()}: {item[1]}"
            # Add newline if it's not last element of list
            if not item == input_list[-1]:
                result += "\n"
        return result
